<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id')->default(0);
            $table->integer('icon_id')->default(0);
            $table->integer('group_id')->default(0);
            $table->integer('market_group_id')->default(0);
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('volume')->nullable();
            $table->string('packaged_volume')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
