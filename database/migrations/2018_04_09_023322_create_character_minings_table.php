<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterMiningsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('character_minings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('character_id')->nullable();
            $table->string('solar_system_id')->nullable();
            $table->string('type_id')->nullable();
            $table->string('quantity')->nullable();
            $table->string('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('character_minings');
    }

}
