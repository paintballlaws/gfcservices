<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatebjobsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('bjobs', function (Blueprint $table) {
            $table->increments('id');
			$table->interger('user_id');
            $table->text('item_name');
			$table->biginteger('tips');
            $table->biginteger('price');
            $table->boolean('expedited_job');
            $table->text('comments_from_user');
            $table->text('comments_from_builder');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('bjobs');
    }

}
