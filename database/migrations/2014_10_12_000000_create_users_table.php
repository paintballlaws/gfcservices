<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('character_id')->nullable();
            $table->string('corporation_id')->nullable();
            $table->string('race_id')->nullable();
            $table->string('alliance_id')->nullable();
            $table->string('bloodline_id')->nullable();
            $table->string('ancestry_id')->nullable();
            $table->string('gender', 20)->nullable();
            $table->string('dob')->nullable();
            $table->longText('description')->nullable();
            $table->text('access_token')->nullable();
            $table->text('refresh_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
