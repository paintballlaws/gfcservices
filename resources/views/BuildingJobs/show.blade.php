@extends( 'Layouts.master' )

@section( 'title' )

GFC Services

@endsection

@section( 'content' )

<div class="container" style="text-align: center;">
	<div class="jumbotron" style="background-color: #e9ecef;">

		@include('BuildingJobs.table')
		
	</div>
</div>
@endsection