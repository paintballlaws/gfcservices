@extends ('Layouts.master')

@section('title')

Job

@endsection

@section('content')


		

<div class="jumbotron text-center">
		<div class="container">
      <h2>Edit A Job</h2><br  />
        <form method="post" action="/bjobs/edit/update">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="item_name">Item:</label>
            <select class="custom-select form-control" name="item_name">
				<option selected>{{$buildingjobs->item_name}}</option>
									<optgroup label="Titan">
										<option value="Avatar">Avatar</option>
										<option value="Erebus">Erebus</option>
										<option value="Ragnarok">Ragnarok</option>
									</optgroup>
									
									<optgroup label="Faction Titan">
										<option value="Vanquiser">Vanquisher</option>
    									<option value="Molok">Molok</option>
									</optgroup>
									
									<optgroup label="Super Capital">
										<option value="Nyx">Nyx</option>
										<option value="Aeon">Aeon</option>
										<option value="Hel">Hel</option>
										<option value="Wyvern">Wyvern</option>
									</optgroup>
									
									<optgroup label="Faction Super Capital">
										<option value="Vendetta">Vendetta</option>
										<option value="Revenant">Revenant</option>
									</optgroup>
									
									<optgroup label="Dreadnaught">
    									<option value="Revelation">Revelation</option>
										<option value="Naglfar">Naglfar</option>
									</optgroup>
									
									<optgroup label="Carrier">
										<option value="Nidhoggur">Nidhoggur</option>
										<option value="Thanatos">Thanatos</option>
										<option value="Archon">Archon</option>
										<option value="Chimera">Chimera</option>
									</optgroup>
									
									<optgroup label="Force Auxilary">
										<option value="Archon">Lif</option>
										<option value="Chimera">Apostle</option>
									</optgroup>
  								</select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="status">Status</label>
              <select class="custom-select form-control" name="status">
				  <option selected>{{$buildingjobs->status}}</option>
					<option>Pending Acceptence</option>
					<option>Approved Job</option>
					<option>Collecting Ore/Minerals</option>
					<option>Building Parts</option>
					<option>Building Item</option>
					<option>Item Finished And On Contract</option>
					<option>Contract Finished</option>
					<option>Archived</option>
				</select>
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="builder_comments">Builder Comments</label>
              <input type="text" class="form-control" name="builder_comments" value="{{$buildingjobs->comments_from_builder}}">
            </div>
          </div>
		<div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">Price</label>
              <input type="number" class="form-control" name="price" value="{{$buildingjobs->price}}">
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
          </div>
        </div>
      </form>
    </div>





</div>

@endsection