@extends( 'Layouts.master' )

@section( 'title' )

Order

@endsection

@section( 'content' )

<div class="container" style="text-align: center; padding-top: 125px;">
	<div class="jumbotron" style="background-color: #e9ecef">
		
		<div class=" row">
			<div class="col-lg-12 margin-tb">
				
					<h1>Order Page</h1>
				
					<p>To order something from GFC simply fill out this form and then we will review your order and start making it as soon as possible. We offer all Titans except the Leviathan, We offer all Super Carriers, Carriers, Faxes, and Dreadnaughts.</p>
				
				<hr>
				<br>

				<form method="POST" action="/bjobstore">
					@csrf

					<div class="form-group row">
  						<label class="col-sm-4 col-form-label" for="item_name">Item's</label>
  							<div class="col-sm-5">
  								<select class="custom-select form-control" id="item_name" name="item_name" required>
    								<option selected>Choose...</option>
									
									<optgroup label="Titan">
										<option value="Avatar">Avatar</option>
										<option value="Erebus">Erebus</option>
										<option value="Ragnarok">Ragnarok</option>
									</optgroup>
									
									<optgroup label="Faction Titan">
										<option value="Vanquiser">Vanquisher</option>
    									<option value="Molok">Molok</option>
									</optgroup>
									
									<optgroup label="Super Capital">
										<option value="Nyx">Nyx</option>
										<option value="Aeon">Aeon</option>
										<option value="Hel">Hel</option>
										<option value="Wyvern">Wyvern</option>
									</optgroup>
									
									<optgroup label="Faction Super Capital">
										<option value="Vendetta">Vendetta</option>
										<option value="Revenant">Revenant</option>
									</optgroup>
									
									<optgroup label="Dreadnaught">
    									<option value="Revelation">Revelation</option>
										<option value="Naglfar">Naglfar</option>
									</optgroup>
									
									<optgroup label="Carrier">
										<option value="Nidhoggur">Nidhoggur</option>
										<option value="Thanatos">Thanatos</option>
										<option value="Archon">Archon</option>
										<option value="Chimera">Chimera</option>
									</optgroup>
									
									<optgroup label="Force Auxilary">
										<option value="Archon">Lif</option>
										<option value="Chimera">Apostle</option>
									</optgroup>
  								</select>
						</div>
					</div>
	
					<div class="form-group row">
						<label for="tips" class="col-sm-4 col-form-label">Tip Jar</label>
						<div class="col-sm-5">
							<input type="number" class="form-control" id="tips" name="tips">
						</div>
					</div>
					<div class="form-group row">
						<label for="expedited_job" class="col-sm-4 col-form-label">Rush Job</label>
						<div class="col-sm-5">

							<div class="form-check">
								<input class="form-check-input " type="checkbox" id="expedited_job" name="expedited_job">
								<label class="form-check-label " for="expedited_job ">This Add's A %2 Fee To The Final Total</label>
							</div>
						</div>
					</div>


					<div class="form-group row">
						<label for="price" class="col-sm-4 col-form-label">Price</label>
						<div class="col-sm-5">
							<input type="number" class="form-control" id="price" disabled name="price" placeholder="Builder Will Determine Price After Order Is Submitted">
						</div>
					</div>


					<div class="form-group row">
						<div class="col-sm-12">
							<label for="comments" class="col-sm-10 col-form-label">Comments</label>
							<textarea name="comments_from_user" id="comments_from_user" class="form-control" placeholder="Please Say Something In Here If You Have A Special Request!"></textarea>
						</div>
					</div>

					<div class=" row">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary">Submit Order</button>
						</div>
					</div>

				</form>
				
			</div>
		</div>
	</div>
</div>
@endsection