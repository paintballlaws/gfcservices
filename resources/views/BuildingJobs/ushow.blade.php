@extends ('Layouts.master')

@section('title')

Job

@endsection

@section('content')


		

<div class="jumbotron text-center">
		<div class="container">
      <h2>Edit A Form</h2><br  />
        <form method="post" action="/bjobs/edit/update">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="item_name">Item:</label>
            <input type="text" class="form-control" name="item_name" value="{{$buildingjobs->item_name}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="status">Status</label>
              <input type="text" class="form-control" name="status" value="{{$buildingjobs->status}}">
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="builder_comments">Builder Comments</label>
              <input type="text" class="form-control" name="builder_comments" value="{{$buildingjobs->comments_from_builder}}">
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
          </div>
        </div>
      </form>
    </div>





</div>

@endsection