<div class="row">
    <div class="col-xl-12">
        @include('backend.common.flash')
     
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table m-table m-table--head-separator-primary">
                            <thead>
                                <tr>
                                    <th>Order ID</th>
									<th>Character</th>
                                    <th>Item Name</th>
                                    <th>User Comments</th>
                                    <th>Job Status</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($buildingjobs as $buildingjob)
                                <tr>
                                    <td>{{$buildingjob->id}}</td>
									<td>{{$buildingjob->user->name}}</td>
									<td>{{$buildingjob->item_name}}</td>
                                    <td>{{$buildingjob->comments_from_user }}</td>
                                    <td>{{$buildingjob->status}}</td>
									<td width="100px">{{$buildingjob->created_at->toFormattedDateString()}}</td>
									<td><a href="{{action('bjobsController@edit', $buildingjob['id'])}}" class="btn btn-warning">Edit</a></td>
									<td>
          								<form action="{{action('bjobsController@destroy', $buildingjob['id'])}}" method="post">
            								@csrf
            								<input name="_method" type="hidden" value="DELETE">
            									<button class="btn btn-danger" type="submit">Delete</button>
          								</form>
       							 	</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <!--end::Form-->
        </div>