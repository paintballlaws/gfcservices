@extends ('Layouts.master')
@section('title', 'GFC Services')
@section('content')
<div class="container">
    <div class="row margin-top-75">
        <div class="col-md-12">
            <center>
                <img src="//image.eveonline.com/Character/{{ auth()->user()->character_id }}_128.jpg" name="aboutme" width="140" height="140" border="0" class="img-circle"></a>
				<br>
                <h3 class="media-heading">{{ auth()->user()->name }}</h3>
				<br>
				<h4 class="media-heading">Group:&nbsp;{{ ucfirst(auth()->user()->group) }}</h4>
            </center>
            <hr>
            <ul class="nav justify-content-center nav-pills">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}?mining=character">Character Mining</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}?mining=corporation">Corporation Mining</a>
                </li>
            </ul>
            <br/>
            <div class="panel panel-default">
                <div class="panel-body">
                    @if($mining_data)
                    <table class="table mining-table">
                        <thead>
                            <tr>
                                <th>Timestamp</th>
                                <th>Ore Type</th>
                                <th>Quantity</th>
                                <th>{{ $mining == 'character' ? 'Solar System':'Pilot' }}</th>
                                <th>Volume</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($mining_data as $chr_mining_data)
                            <tr>
                                <td>{{ $chr_mining_data['timestamp']  }}</td>
                                <td>{{ $chr_mining_data['ore_type']  }}</td>
                                <td>{{ $chr_mining_data['quantity']  }}</td>
                                <td>{{ $mining == 'character' ? $chr_mining_data['solar_system']:$chr_mining_data['pilot'] }}</td>
                                <td>{{ $chr_mining_data['volume']  }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="col-md-12 text-center">No mining data.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection