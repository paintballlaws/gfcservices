@extends ('Layouts.master')
@section('title', 'Not Authorized')
@section('content')

<div class="container" style="text-align: center; padding-top: 125px;">
    <div class="jumbotron" style="background-color: #e9ecef">

        <h1 class="alert alert-danger" style="text-decoration: underline;">You Are Not Authorized</h1>

        <br>
		<img src="/images/404.jpg" style="width: 900px; height: 600px;">
        <br>
		<br>
        <div class="container" style=" text-align: center;">
            <h2>To View This Page Please Contact An Administrator To Receive The Proper Roles</h2>
			<h4 class="media-heading">Your Current Group:&nbsp;{{ ucfirst(auth()->user()->group) }}</h4>
        </div>

    </div> 
</div>

@endsection