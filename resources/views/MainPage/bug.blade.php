@extends('Layouts.master')
@section('title','Bug Reports')
@section('content')

<div class="container" style="text-align: center; padding-top: 125px;">
	<div class="jumbotron" style="background-color: #e9ecef">
		<img src="/images/cockroach.jpg" style="width: 175px; height: 175px;">
		<div class=" row">
			<div class="col-lg-12 margin-tb">
				
					<h2>Bug Reporting</h2>
					<p>Please Report Any Bugs Or Errors That You Find!</p>
				
				<hr>
				<br>

				<form method="POST" action="/bugs">
					{{ csrf_field() }}


					<div class="form-group row">
						<label for="item_name" class="col-sm-12 col-form-label">Title</label>
						<div class="col-sm-12">
							<input type="text" class="form-control " id="title" name="title" required>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-sm-12 col-form-label" for="bugpriority">Priority</label>
						<select class="col-sm-12" id="bugpriority">
						<option>High</option>
						<option>Medium</option>
						<option>Low</option>
						<option>Feature Request</option>
						</select>
					</div>

					<div class="form-group row">
						<div class="col-sm-12">
							<label for="comments" class="col-sm-10 col-form-label">Bug Description</label>
							<textarea name="bugreportcom" id="bugreportcom" class="form-control" placeholder="Description of bug or error and how to recreate it."></textarea>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>

				</form>

			</div>
		</div>
	</div>
</div>


@endsection