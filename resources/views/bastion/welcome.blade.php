@extends( 'Layouts.master' )
@section( 'title', 'Bastion' )
@section( 'content' )

<div class="container" style="text-align: center; padding-top: 125px;">
	<div class="jumbotron" style="background-color: #e9ecef">
		<img src="/images/BASTION.png" style="top: auto">

		<h1 style="text-decoration: underline;">General Info For The Alliance</h1>

		<br>
		<br>

		<div class="row" style="text-align: center">
			<div class="col-md-6 mb-3">
				<h4 class="mb-3" style="text-decoration: underline">Links To Bastion Services</h2>
											<div class="container col-md-7" style="text-align: left">
										<li><a href="http://rampart.thebastion.info" target="_blank">Rampart(API's And SIG's)</a>
										</li>
										<li><a href="http://confluence.thebastion.info" target="_blank">Confluence(Wiki)</a>
										</li>
										<li><a href="http://forums.thebastion.info" target="_blank">Forum's</a>
										</li>
										<li><a href="http://srp.thebastion.info" target="_blank">Ship Replacment Program</a>
										</li>
										<li><a href="http://crap.thebastion.info" target="_blank">Capital Application Program</a>
										</li>
										<li><a href="https://zkillboard.com/alliance/99004425/" target="_blank">zKillboard</a>
										</li>
										<li><a href="https://adashboard.info" target="_blank">aDashboard(Pap's)</a>
										</li>
										</div>
									
								</div>
									<div class="col-md-6 mb-3">
										<h4 class="mb-3" style="text-decoration: underline">Links To Grass Fed Cannibals Services</h4>
				<div class="container col-md-7" style="text-align: left">
										<li><a href="https://zkillboard.com/corporation/98467413/" target="_blank">zKillboard</a>
										</li>
										<li><a href="http://seat.gfcservices.info" target="_blank">GFC Seat Server(API's)</a>
										</li>
					
					
					
				</div>

			</div>
			
									<div class="col-md-6 mb-3">
										<h4 class="mb-3" style="text-decoration: underline">Links To Goonswarm Services</h4>
										<div class="container col-md-7" style="text-align: left">
											<li><a href="https://goonfleet.com/" target="_blank">Goonfleet(Forums)</a>
											</li>
											<li><a href="https://wiki.goonfleet.com/Main_Page" target="_blank">Goon Wiki</a>
											</li>
											<li><a href="https://gice.goonfleet.com" target="_blank">Goon ESI Registration</a>
											</li>
											<li><a href="https://goonfleet.com/esa/" target="_blank">Goon ESA(Mumble Stuff)</a>
											</li>
											<li><a href="https://goonfleet.com/index.php/forum/218-super-s-mart/" target="_blank">S-Mart(Super-Cap Selling)</a>
											</li>
										</div>
			</div>
		</div>


	</div>
</div>

@endsection