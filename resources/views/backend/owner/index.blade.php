@extends('backend.layouts.main')
@section('title', 'Dashboard')
@section('content')
<div class="row">
    <div class="col-xl-12">
        @include('backend.common.flash')
        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head m-portlet__head--fit-">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Users
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link{{ $group == 'all' ? ' active':'' }}" href="{{ route('owner.index') }}" role="tab">
                                All
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link{{ $group == 'guest' ? ' active':'' }}" href="{{ route('owner.index') }}?group=guest" role="tab">
                                Guest
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link{{ $group == 'member' ? ' active':'' }}" href="{{ route('owner.index') }}?group=member" role="tab">
                                Member
                            </a>
                        </li>
						<li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link{{ $group == 'blue' ? ' active':'' }}" href="{{ route('owner.index') }}?group=blue" role="tab">
                                Blue
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link{{ $group == 'builder' ? ' active':'' }}" href="{{ route('owner.index') }}?group=builder" role="tab">
                                Builder
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link{{ $group == 'admin' ? ' active':'' }}" href="{{ route('owner.index') }}?group=admin" role="tab">
                                Admin
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table m-table m-table--head-separator-primary">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Group</th>
                                    <th>Character ID</th>
                                    <th>Corporation ID</th>
                                    <th>Alliance ID</th>
                                    <th>Race ID</th>
                                    <th>Bloodline ID</th>
                                    <th>Gender</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ ucfirst($user->group) }}</td>
                                    <td>{{ $user->character_id }}</td>
                                    <td>{{ $user->corporation_id }}</td>
                                    <td>{{ $user->alliance_id }}</td>
                                    <td>{{ $user->race_id }}</td>
                                    <td>{{ $user->bloodline_id }}</td>
                                    <td>{{ $user->gender }}</td>
                                    <td><a href="{{ route('owner.user.edit', $user->id) }}" class="btn m-btn--square  btn-primary btn-sm">Edit</a></td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="9"><span class="pull-right">{{ $users->appends(['group' => $group])->links() }}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
</div>
@endsection