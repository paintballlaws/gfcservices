@extends('backend.layouts.main')
@section('title', 'Edit User')
@section('content')
<div class="row">
    <div class="col-md-6">
        <!--begin::Portlet-->
        @include('backend.common.flash')
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Edit ({{ $user->name }})
                        </h3>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form method="POST" action="{{ route('owner.user.update', $user->id) }}" class="m-form m-form--fit m-form--label-align-right">
                @csrf
                <div class="m-portlet__body">
                    <div class="form-group m-form__group">
                        <label>Change Group</label>
                        <select name="group" class="form-control m-input">
                            <option value="guest">Guest</option>
                            <option value="member">Member</option>
							<option value="member">Blue</option>
                            <option value="builder">Builder</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-metal">Change Group</button>
                    </div>
                </div>
            </form>
            <!--end::Form-->			
        </div>
        <!--end::Portlet-->
    </div>
</div>
@endsection
