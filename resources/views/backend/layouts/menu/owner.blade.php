<li class="m-menu__item{{ $segment == 'owner' ? ' m-menu__item--active':'' }}" aria-haspopup="true">
    <a href="{{ route('owner.index') }}" class="m-menu__link">
        <span class="m-menu__item-here"></span>
        <span class="m-menu__link-text">
            Dashboard
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{ route('logout') }}" class="m-menu__link" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
        <span class="m-menu__item-here"></span>
        <span class="m-menu__link-text">
            Logout
        </span>
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</li>