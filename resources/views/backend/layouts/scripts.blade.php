<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->
<!--begin::Base Scripts -->
<script src="{{ asset('backend/assets/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/base/scripts.bundle.js') }}" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Vendors -->
@yield('page_vendors')
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
@yield('custom_scripts')
<script src="{{ asset('backend/js/common.js') }}" type="text/javascript"></script>
<!--end::Page Snippets -->