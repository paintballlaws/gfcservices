<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>{{ env('APP_NAME') }}: @yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="site-url" content="{{ url('/') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
WebFont.load({
    google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
    active: function () {
        sessionStorage.fonts = true;
    }
});
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{ asset('backend/assets/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/base/style.bundle.css') }}" rel="stylesheet" type="text/css" /> 
    <link href="{{ asset('backend/css/common.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    @yield('custom_styles')
</head>
<!-- end::Head -->