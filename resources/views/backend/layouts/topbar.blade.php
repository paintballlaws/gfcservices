<!-- begin::Topbar -->
<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
        <div class="m-stack__item m-topbar__nav-wrapper">
            <ul class="m-topbar__nav m-nav m-nav--inline">
                @include('backend.layouts.user')
                @include('backend.layouts.notifications')
                @include('backend.layouts.quick_actions')
            </ul>
        </div>
    </div>
</div>
<!-- end::Topbar -->