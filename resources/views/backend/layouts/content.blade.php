<!-- begin::Content -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="col-md-12">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                   @yield('sub_header')
                </div>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        @yield('content')
    </div>
</div>
<!-- end::Content -->