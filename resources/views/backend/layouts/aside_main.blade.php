<!DOCTYPE html>
<html lang="en">
    @include('backend.layouts.head')
    <!-- begin::Body -->

    <body class="m-page--wide m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            @include('backend.layouts.header')
            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
                <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver m-container m-container--responsive m-container--xxl m-page__container">
                    @include('backend.layouts.aside')
                    @include('backend.layouts.content')
                </div>
            </div>
            @include('backend.layouts.footer')
        </div>
        <!-- end:: Page -->
        @include('backend.layouts.scripts')
    </body>
    <!-- end::Body -->

</html>