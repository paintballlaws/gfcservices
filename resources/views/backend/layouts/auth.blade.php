<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->

    <head>
        <meta charset="utf-8" />
        <title>{{ env('APP_NAME') }}: @yield('title')</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
WebFont.load({
    google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
    active: function () {
        sessionStorage.fonts = true;
    }
});
        </script>
        <!--end::Web font -->
        <!--begin::Base Styles -->
        <link href="{{ asset('backend/assets/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('backend/assets/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    </head>
    <!-- end::Head -->
    <!-- end::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page m--align-center">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-2" id="m_login"
                 style="background-image: url({{ asset('images/bg-3.jpg') }});">
                @yield('content')
            </div>
        </div>
        <!-- end:: Page -->
        <!--begin::Base Scripts -->
        <script src="{{ asset('backend/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ asset('backend/assets/demo/demo2/base/scripts.bundle.js') }}" type="text/javascript"></script>
        <!--end::Base Scripts -->
    </body>
    <!-- end::Body -->

</html>
