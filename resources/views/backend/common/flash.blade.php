<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <div class="m-alert m-alert--outline alert alert-{{ $msg }} alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
        {{ Session::get('alert-' . $msg) }}					  	
    </div>
    @endif
    @endforeach
</div>
