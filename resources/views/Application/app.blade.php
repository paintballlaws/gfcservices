@extends( 'Layouts.master' )

@section( 'title' )

Application

@endsection

@section( 'content' )

<div class="container" style="text-align: center; padding-top: 125px;">
	<div class="jumbotron" style="background-color: #e9ecef;">

		<h1 style="text-decoration: underline">Grass Fed Cannibals Recruitment Application</h1>
		<h4 class="alert alert-warning">Please Make Sure You Have Signed Up On Seat With Your API!! Failure To Do So Will Result In Automatic Deletion Of Your Application!!!</h4>
		<h4>This is your application to -GFC. Take your time and give complete, well thought out answers. Laziness is an automatic NO-GO. Your purpose here is to introduce yourself to the corporation, and give us a solid idea of who we are talking to.</h6>

		<hr>


		<form method="POST" action="/application/store">
			{{ csrf_field() }}

			<h1 style="text-decoration: underline">Section 1: General</h1>
			<hr>
			<div class="form-group row">
				<label for="item_name" class="col-sm-4 col-form-label">Email Address</label>
				<div class="col-sm-5">
					<input type="email" class="form-control" id="email" name="email" required>
				</div>
			</div>

			<div class="form-group row">
				<label for="character_name" class="col-sm-4 col-form-label">Main Character Name</label>
				<div class="col-sm-5">
					<input type="text" class="form-control" id="character_name" name="character_name" placeholder="Name of character you are applying with." required>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Tell Us About Yourself</label>
					<textarea name="aboutuser" id="aboutuser" class="form-control" required placeholder="Whats your name? How old are you? What do you do? What's your primary language? Take a moment and introduce yourself nice and proper."></textarea>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Why do you want to join -GFC?</label>
					<textarea name="whyjoin" id="whyjoin" class="form-control" required></textarea>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">What do you hope to gain from -GFC and The Bastion as a a whole?</label>
					<textarea name="whatgain" id="whatgain" class="form-control" required></textarea>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">What skills and abilities do you bring for -GFC and The Bastion to utilize?</label>
					<textarea name="whatskills" id="whatskills" class="form-control" required></textarea>
				</div>
			</div>

			<fieldset class="form-group">
				<div class="row">
					<legend class="col-sm-12 col-form-label">Please check which ever aspects of Eve intrerest you:</legend>
					<div class="col-sm-12">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="pvp" id="pvp" value="pvp">
							<label class="form-check-label" for="gridRadios1">
            PvP (General)
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="pve" id="pve" value="pve">
							<label class="form-check-label" for="gridRadios2">
            PvE
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="smallgang" id="smallgang" value="small-gang">
							<label class="form-check-label" for="gridRadios2">
            Small Gang
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="industry" id="industry" value="industry">
							<label class="form-check-label" for="gridRadios2">
            Indusrty
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="lfleets" id="lfleets" value="large_fleets">
							<label class="form-check-label" for="gridRadios2">
            Large scale fleet OP's
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="spvp" id="spvp" value="solo_pvp">
							<label class="form-check-label" for="gridRadios2">
            Solo PvP
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="explor" id="explor" value="exploration">
							<label class="form-check-label" for="gridRadios2">
            Exploration
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="cap_pvp" id="cap_pvp" value="cap_pvp">
							<label class="form-check-label" for="gridRadios2">
            Capital PvP
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="logistics" id="logistics" value="logistics">
							<label class="form-check-label" for="gridRadios2">
            Logistics & Transportation
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="market" id="market" value="market">
							<label class="form-check-label" for="gridRadios2">
            Marketeering
          </label>
						
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="othercheck" id="othercheck" value="othercheck">
							<label class="form-check-label" for="gridRadios2">
            Other
          </label>
						
						</div>
						<div class="form-group row">
							<div class="col-sm-12">
								<label for="comments" class="col-sm-10 col-form-label">If you checked 'Other' in the previous question, please elaborate.</label>
								<textarea name="other" id="other" class="form-control"></textarea>
							</div>
						</div>
					</div>
				</div>
			</fieldset>

			<h1 style="text-decoration: underline">Section 2: Capital Ships</h1>
			<p>If you or one of your toons is a cap pilot, please answer this section. If not, move ahead to the next section.</p>
			<hr>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">What Is Your Preferred Capital Ship?</label>
					<textarea name="preferredcap" id="preferredcap" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Please list each capital ship you can fly (not just sit in, but combat fit)</label>
					<textarea name="listcapsfly" id="listcapsfly" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Is your capital pilot your main or a separate toon? If separate please name and please put in API in SEAT.</label>
				
					<textarea name="altcap" id="altcap" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Please give us a fitting for whichever Capital you plan on flying in combat the most in EFT format. (HINT: It should be a good fit)</label>
					<textarea name="fitcap" id="fitcap" class="form-control"></textarea>
				</div>
			</div>
			<h1 style="text-decoration: underline">Section 3: Leadership Skills</h1>
			<hr>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Please describe any time you took up a leadership or FC position in your eve career. </label>
					<textarea name="tookleader" id="tookleader" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">If you have never taken a leadership or FC position, would you be willing to?</label>
					<textarea name="willingleader" id="willingleader" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Please describe a situation, if any, when you stepped up and solved a problem for your corp.</label>
					<textarea name="sitcorp" id="sitcorp" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Write us a limerick (yes a GOD DAMN limerick) about why Paintballlawss' ass is so incredibly fat. Dunno what a limerick is? Google that shit.</label>
					<textarea name="limepaint" id="limepaint" class="form-control" required></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Give us a short thesis on why gimmie the Jita is such a whiny, cancerous bitch. Just guess. Be humorous :)</label>
					<textarea name="samcancer" id="samcancer" class="form-control" required></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<label for="comments" class="col-sm-10 col-form-label">Please visit <a href="https://bad-dragon.com/" target="_blank">Bad Dragon</a> and pick your favorite and then tell us why!</label>
					<textarea name="baddragon" id="baddragon" class="form-control" required></textarea>
				</div>
			</div>

			<div class=" row">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-primary">Submit My Application</button>
				</div>
			</div>

		</form>

	</div>
</div>

@endsection