<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>

            @yield('title')

        </title>
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/css/album.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		
    <script>
WebFont.load({
    google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
    active: function () {
        sessionStorage.fonts = true;
    }
});
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{ asset('backend/assets/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/assets/base/style.bundle.css') }}" rel="stylesheet" type="text/css" /> 
    <link href="{{ asset('backend/css/common.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
        @yield('style')
        <style>
            body {
                margin: 0;
                padding: 0;
            }

            #header {
                height: 40px;
                padding: 20px;
                border-bottom: 1px;
            }

            #main {
                padding: 15px;
                margin-bottom: 60px;
            }

            #footer {
                height: 50px;
                padding: 10px;
                bottom: 0;
                width: 100%;
                z-index: -1;
            }
            .margin-top-75 {
                margin-top: 75px;
            }
            .mining-table td, .mining-table th {
                text-align: center;
            }
        </style>

    </head>

    <body>
        <div class="wrapper">
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <img src="/images/GFC.png" style="height: 40px; width: 40px"></img>
                <a class="navbar-brand" href="/">GFC Services</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
						
						
            			
                        <li class="nav-item">
                            <a class="nav-link" href="/bjobs/create">Order</a>
                        </li>
						
                        
						<li class="nav-item">
                            <a class="nav-link" href="/bjobs/show">Show Building</a>
                        </li>
						
						
						
						<li class="nav-item">
                            <a class="nav-link" href="/bastion">Bastion</a>
                        </li>
						
						
						<!--
                        <li class="nav-item">
                            <a class="nav-link" href="">Application To GFC</a>
                        </li>
						-->
						<li class="nav-item">
                            <a class="nav-link" href="http://seat.gfcservices.info" target="_blank">SeAT Website</a>
                        </li>
						
						
                    </ul>
					
					
					
                    <ul class="nav navbar-nav navbar-right">
                        @if(auth()->check())
                        <li class="nav-item" style="color: #fff; margin-right: 10px"><a href="{{ route('home') }}">{{ auth()->user()->name }}</a></li>
                        <li class="nav-item">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        @else
                        <li><a href="{{ route('auth.redirect') }}"><img src="/images/eve-white-small.png"/></a>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>

            @yield('content')

        </div>

        <footer class="text-muted">

            <div class="container" align="left">
                <a href="http://www.thebastion.info" target="_blank" style="float: right; clear: right;"><img src="/images/BASTION.png" style="height: 50px; width: 50px;"></img>The Bastion</a> &copy;
                <?php echo date("Y"); ?> Grass Fed Cannibals - <a href="/changelog">Changelog v1.0.0</a>--<a style="align-content: center" href="/bugreport">Bug Reporting
			
            </div>
        </footer>

    </body>
    <!-- Bootstrap core JavaScript
        ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</html>