@extends('backend.layouts.auth')
@section('title', 'Sign In')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
    <div class="m-login__container">
        <div class="m-login__logo">
            <a href="{{ route('welcome') }}">
                <img alt="" src="{{ asset('backend/images/logo128.png') }}" />
            </a>
        </div>
        <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    Sign In
                </h3>
            </div>
            <form method="post" class="m-login__form m-form" action="{{ url('login') }}">
                {{ csrf_field() }}
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="Email" name="email" value="{{ old('email') }}" autocomplete="off">
                    <em class="m--font-danger m--font-boldest">
                        @if ($errors->has('email')) {{ $errors->first('email') }} @endif
                    </em>
                </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                    <em class="m--font-danger m--font-boldest">
                        @if ($errors->has('password')) {{ $errors->first('password') }} @endif
                    </em>
                </div>
                <div class="row m-login__form-sub">
                    <div class="col m--align-left m-login__form-left">
                        <label class="m-checkbox  m-checkbox--focus">
                            <input type="checkbox" name="remember"> Remember me
                            <span></span>
                        </label>
                    </div>
                    <div class="col m--align-right m-login__form-right">
                        <a href="{{ url('/password/reset') }}" id="m_login_forget_password" class="m-link">
                            Forget Password ?
                        </a>
                    </div>
                </div>
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                        Sign In
                    </button>
                </div>
                <hr/>
                <a href="{{ route('auth.redirect') }}"><img src="/images/eve-white-small.png"/>
            </form>
        </div>
        <!--
        <div class="m-login__account">
            <span class="m-login__account-msg">
                Don't have an account yet ?
            </span>
            &nbsp;&nbsp;
            <a href="{{ url('register') }}" id="m_login_signup" class="m-link m-link--light m-login__account-link">
                Sign Up
            </a>
        </div>
        -->
    </div>
</div>
@endsection
