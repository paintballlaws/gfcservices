@extends('backend.layouts.auth')
@section('title', 'Sign Up')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
    <div class="m-login__container">
        <div class="m-login__logo">
            <a href="{{ route('welcome') }}">
                <img alt="" src="{{ asset('backend/images/logo128.png') }}" />
            </a>
        </div>
        <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    Sign Up
                </h3>
            </div>
            <form method="post" class="m-login__form m-form" action="{{ url('register') }}">
                {{ csrf_field() }}
                <div class="form-group m-form__group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input class="form-control m-input" type="text" placeholder="Your Name or Business Name" name="name" value="{{ old('name') }}" autocomplete="off">
                    <em class="m--font-danger m--font-boldest">
                        @if ($errors->has('name')) {{ $errors->first('name') }} @endif
                    </em>
                </div>
                <div class="form-group m-form__group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="form-control m-input" type="text" placeholder="Email Address" name="email" value="{{ old('email') }}" autocomplete="off">
                    <em class="m--font-danger m--font-boldest">
                        @if ($errors->has('email')) {{ $errors->first('email') }} @endif
                    </em>
                </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                    <em class="m--font-danger m--font-boldest">
                        @if ($errors->has('password')) {{ $errors->first('password') }} @endif
                    </em>
                </div>
                <div class="form-group m-form__group">
                    <input id="password-confirm" class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password_confirmation">
                </div>
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                        Sign Up
                    </button>
                </div>
            </form>
        </div>
        <div class="m-login__account">
            <span class="m-login__account-msg">
                Already have an account?
            </span>
            &nbsp;&nbsp;
            <a href="{{ route('login') }}" id="m_login_signup" class="m-link m-link--light m-login__account-link">
                Sign In
            </a>
        </div>
    </div>
</div>
@endsection