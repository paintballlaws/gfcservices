@extends('backend.layouts.auth')
@section('title', 'Sign In')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
    <div class="m-login__container">
        <div class="m-login__logo">
            <a href="{{ route('welcome') }}">
                <img alt="" src="{{ asset('backend/images/logo128.png') }}" />
            </a>
        </div>
        <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    Reset Password
                </h3>
            </div>
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <form method="post" class="m-login__form m-form" action="{{ route('password.request') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="Email" name="email" value="{{ old('email') }}" autocomplete="off">
                    <em class="m--font-danger m--font-boldest">
                        @if ($errors->has('email')) {{ $errors->first('email') }} @endif
                    </em>
                </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                    <em class="m--font-danger m--font-boldest">
                        @if ($errors->has('password')) {{ $errors->first('password') }} @endif
                    </em>
                </div>
                <div class="form-group m-form__group">
                    <input id="password-confirm" class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password_confirmation">
                </div>
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                        Reset Password
                    </button>
                </div>
            </form>
        </div>
        <!--
        <div class="m-login__account">
            <span class="m-login__account-msg">
                Don't have an account yet ?
            </span>
            &nbsp;&nbsp;
            <a href="{{ route('register') }}" id="m_login_signup" class="m-link m-link--light m-login__account-link">
                Sign Up
            </a>
        </div>
        -->
    </div>
</div>
@endsection