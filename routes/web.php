<?php

// Landing Page
Route::get('/', 'MainPageController@index')->name('welcome');

// EveOnline Login Redirect Url
Route::get('auth/redirect', 'ConnectController@authUrl')->name('auth.redirect');

// EveOnline OAuth Callback
Route::get('auth/callback', 'ConnectController@callback')->name('auth.callback');

// Owner Middleware Routes
Route::group(['prefix' => 'owner', 'middleware' => ['auth', 'owner'], 'as' => 'owner.'], function() {
    Route::get('/', 'Owner\OwnerController@index')->name('index');
    Route::get('user/edit/{id}', 'Owner\OwnerController@edit')->name('user.edit');
    Route::post('user/update/{id}', 'Owner\OwnerController@update')->name('user.update');
});


//Show Bjobs Middleware Routes
Route::group(['prefix' => 'bjobs', 'middleware' => ['showbuilding','auth']], function() {
	Route::get('/', 'bjobsController@index')->name('index');
	Route::get('bjob/edit/{id}', 'bjobsController@edit')->name('bjob.edit');
	Route::post('bjob/update/{id}', 'bjobsController@update')->name('bjob.update');
	
	});

//Create Middleware Routes
Route::group(['middleware' => ['order','auth']], function() {
    Route::get('/bjobs/create', 'bjobsController@create');
	
	});
//Bastion Middleware Routes
Route::group(['middleware' => ['bastion','auth']], function() {
Route::get('/bastion', 'MainPageController@bastion');
	
	});

Route::get('test', function() {
    $esi = new \Sumbria\Esi\Esi;
    $esi->setAccessToken(generate_token());
    $character_id = auth()->user()->character_id;
    $industry_obj = new \Sumbria\Esi\Industry();
    $character_mining = $industry_obj->getCharacterMining($character_id);
    if (is_array($character_mining['data']) && count($character_mining['data']) > 0) {
        foreach ($character_mining['data'] as $chr_mining) {
            
        }
    }
});

//Changelog
Route::get('/changelog', 'MainPageController@changelog');

//Middleware to store jobs
Route::group(['middleware' => 'auth'], function() {
    Route::post('/bjobstore', 'bjobsController@store');
});

//Showing Jobs
Route::group(['middleware' => ['showbuilding','auth']], function() {
	Route::get('/bjobs/show', 'bjobsController@show');
});

//Showing Individual Jobs
Route::group(['middleware' => ['showbuilding','auth']], function() {
	Route::get('/bjobs/show/{id}', 'bjobsController@ushow');
});

//Deleting Jobs
Route::group(['middleware' => ['showbuilding','auth']], function() {
	Route::get('/bjobs/show/delete', 'bjobsController@destroy');
});

//Updating Job
Route::group(['middleware' => ['showbuilding','auth']], function() {
	Route::get('/bjobs/edit/update', 'bjobsController@update');
});
	
//Test Searching Jobs
Route::get('/item/{searchTerm}', 'bjobsController@lookupitemname');


//Application Routes
Route::get('/application', 'ApplicationController@index');
Route::get('/application/store', 'ApplicationController@store');


//Auth Routes
Auth::routes();

//User Dashboard
Route::get('/home', 'HomeController@index')->name('home');

//Submit Bug Reports
Route::get('/bugreport', 'MainPageController@bug');


//Not Authed For That Page
Route::get('/noauth', 'MainPageController@noauth')->name('noauth');






