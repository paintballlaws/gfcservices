<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharacterMining extends Model {

    protected $fillable = [
        'character_id', 'solar_system_id', 'type_id', 'quantity', 'date'
    ];

    public function character() {
        return $this->hasOne('App\User', 'character_id', 'character_id');
    }

    public function system() {
        return $this->hasOne('App\System', 'system_id', 'solar_system_id');
    }

    public function type() {
        return $this->hasOne('App\Type', 'type_id', 'type_id');
    }

}
