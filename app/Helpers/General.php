<?php

if (!function_exists('pr')) {

    function pr($e) {
        echo '<pre>';
        print_r($e);
        echo '</pre>';
    }

}

if (!function_exists('vd')) {

    function vd() {
        foreach (func_get_args() as $e) {
            echo "<pre>";
            var_dump($e);
            echo "</pre>";
        }
    }

}

if (!function_exists('jl')) {

    function jl($e, $loc = __DIR__, $file_name = '', $raw_log = false) {
        $raw_log = $raw_log === true;
        if (!is_dir($loc)) {
            $loc = __DIR__;
        }

        if (!$file_name) {
            $file_name = 'log' . (!$raw_log ? '.json' : '');
        }
        $log_data = $raw_log ? print_r($e, true) : @json_encode($e, JSON_PRETTY_PRINT);
        @error_log($log_data . "\n\n", 3, $loc . "/{$file_name}");
    }

}

if (!function_exists('lg')) {

    function lg($e, $loc = __DIR__, $file_name = '') {
        jl($e, $loc, $file_name, true);
    }

}

if (!function_exists('jc')) {

    function jc($data, $loc = __DIR__, $file_name = 'log.json') {
        $json = json_encode($data, JSON_PRETTY_PRINT);
        file_put_contents($loc . "/{$file_name}", $json);
    }

}


if (!function_exists('generate_token')) {

    function generate_token() {
        $eveonline = new \Sumbria\EveOnline\EveOnline;
        $eveonline->setClientId(env('EVEONLINE_CLIENT_ID'));
        $eveonline->setClientSecret(env('EVEONLINE_CLIENT_SECRET'));
        $eveonline->generateAccessTokenByRefreshToken(auth()->user()->refresh_token);
        $access_token = $eveonline->getAccessToken();
        $refresh_token = $eveonline->getRefreshToken();
        $user = auth()->user();
        $user->access_token = $access_token;
        $user->refresh_token = $refresh_token;
        $user->save();
        return $access_token;
    }

}
