<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model {

    protected $fillable = [
        'type_id', 'icon_id', 'group_id', 'market_group_id', 'name', 'description', 'volume', 'packaged_volume'
    ];

}
