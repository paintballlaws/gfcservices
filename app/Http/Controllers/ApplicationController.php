<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Application;


class ApplicationController extends Controller
{
    
	
	public function index()
	{
		
		return view('Application.app');
		
		
	}
	
	public function store(Request $request){
		
		$post = new corpapplications;
		
		$post->email = $request['email'];
		$post->character_name = $request['character_name'];
		$post->aboutuser = $request['aboutuser'];
		$post->whyjoin = $request['whyjoin'];
		$post->whatgain = $request['whatgain'];
		$post->whatskills = $request['whatskills'];
		$post->pvp = $request['pvp'] == "on" ?1:0;
		$post->pve = $request['pve'] == "on" ?1:0;
		$post->smallgang = $request['smallgang'] == "on" ?1:0;
		$post->industry = $request['industry'] == "on" ?1:0;
		$post->lfleets = $request['lfleets'] == "on" ?1:0;
		$post->spvp = $request['spvp'] == "on" ?1:0;
		$post->explor = $request['explor'] == "on" ?1:0;
		$post->cap_pvp = $request['cap_pvp'] == "on" ?1:0;
		$post->logistics = $request['logistics'] == "on" ?1:0;
		$post->market = $request['market'] == "on" ?1:0;
		$post->othercheck = $request['othercheck'] == "on" ?1:0;
		$post->other = $request['other'];
		$post->preferredcap = $request['preferredcap'];
		$post->listcapsfly = $request['listcapsfly'];
		$post->altcap = $request['altcap'];
		$post->fitcap = $request['fitcap'];
		$post->tookleader = $request['tookleader'];
		$post->willingleader = $request['willingleader'];
		$post->sitcorp = $request['sitcorp'];
		$post->limepaint = $request['limepaint'];
		$post->samcancer = $request['samcancer'];
		$post->baddragon = $request['baddragon'];
		
		$post->save();
		session()->flash('alert-success', 'Job Is In The Queue');
		return redirect('/');
		
		
		
		
	}
}