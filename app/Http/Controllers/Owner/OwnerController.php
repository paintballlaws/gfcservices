<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class OwnerController extends Controller {

    public function index() {
        $group = isset($_GET['group']) ? $_GET['group'] : 'all';
        if ($group == 'guest') {
            $users = User::where('group', 'guest')->paginate(20);
        } elseif ($group == 'member') {
            $users = User::where('alliance_id', '99004425')->paginate(20);
        } elseif ($group == 'blue') {
            $users = User::where('alliance_id', '1354830081')->paginate(20); 
		} elseif ($group == 'builder') {
            $users = User::where('group', 'builder')->paginate(20);
        } elseif ($group == 'admin') {
            $users = User::where('group', 'admin')->paginate(20);
        } else {
            $users = User::where('group', '!=', 'owner')->paginate(20);
        }
        return view('backend.owner.index', compact('users', 'group'));
    }

    public function edit($id) {
        $user = User::find($id);
        if ($user) {
            return view('backend.owner.edit', compact('user'));
        } else {
            session()->flash('alert-danger', 'Something went wrong. Please try again.');
            return redirect()->route('owner.index');
        }
    }

    public function update($id, Request $request) {
        $user = User::find($id);
        if ($user) {
            $user->group = $request->group;
            $user->save();
            session()->flash('alert-success', 'User group updated.');
            return redirect()->route('owner.user.edit', $id);
        } else {
            session()->flash('alert-danger', 'Something went wrong. Please try again.');
            return redirect()->route('owner.index');
        }
    }

}
