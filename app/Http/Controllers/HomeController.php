<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sumbria\Esi\Industry;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (auth()->user()->role == 'owner') {
            return redirect()->route('owner.index');
        }
        $mining = isset($_GET['mining']) ? $_GET['mining'] : 'character';
        $esi = new \Sumbria\Esi\Esi;
        $esi->setAccessToken(generate_token());
        $mining_data = [];
        if ($mining == 'character') {
            $character_id = auth()->user()->character_id;
            $industry_obj = new Industry();
            $character_mining = $industry_obj->getCharacterMining($character_id);
            if (isset($character_mining['data'])) {

                if (is_array($character_mining['data']) && count($character_mining['data']) > 0) {
                    foreach ($character_mining['data'] as $chr_mining) {
                        $solar_system = \App\System::whereSystemId($chr_mining->solar_system_id)->first()->name;
                        $ore_type = '';
                        $volume = '';
                        $type = \App\Type::whereTypeId($chr_mining->type_id)->first();
                        if ($type) {
                            $ore_type = $type->name;
                            $volume = $type->volume;
                        } else {
                            $universe = new \Sumbria\Esi\Universe;
                            $type_res = $universe->getType($chr_mining->type_id);
                            $ore_type = $type_res['data']->name;
                            $volume = $type_res['data']->volume;
                            $this->createType($type_res['data']);
                        }
                        $mining_data[] = [
                            'timestamp' => $chr_mining->date,
                            'quantity' => $chr_mining->quantity,
                            'solar_system' => $solar_system,
                            'ore_type' => $ore_type,
                            'volume' => $volume,
                        ];
                    }
                }
            } else {
                return redirect()->route('home');
            }
        }
        if ($mining == 'corporation') {
            $corporation_id = auth()->user()->corporation_id;
            $industry_obj = new Industry();
            $observers = $industry_obj->getCorporationObservers($corporation_id);
            if (isset($observers['data'])) {
                if (is_array($observers['data']) && count($observers['data']) > 0) {
                    foreach ($observers['data'] as $observer) {
                        $obj = new Industry();
                        $corporation_mining = $obj->getObserver($corporation_id, $observer->observer_id);
                        foreach ($corporation_mining['data'] as $corp_mining) {
                            $ore_type = '';
                            $volume = '';
                            $type = \App\Type::whereTypeId($corp_mining->type_id)->first();
                            if ($type) {
                                $ore_type = $type->name;
                                $volume = $type->volume;
                            } else {
                                $universe = new \Sumbria\Esi\Universe;
                                $type_res = $universe->getType($corp_mining->type_id);
                                $ore_type = $type_res['data']->name;
                                $volume = $type_res['data']->volume;
                                $this->createType($type_res['data']);
                            }
                            $esi_character_obj = new \Sumbria\Esi\Character;
                            $esi_character = $esi_character_obj->getCharacter($corp_mining->character_id);
                            $mining_data[] = [
                                'timestamp' => $corp_mining->last_updated,
                                'quantity' => $corp_mining->quantity,
                                'pilot' => $esi_character['data']->name,
                                'ore_type' => $ore_type,
                                'volume' => $volume,
                            ];
                        }
                    }
                }
            } else {
                return redirect()->route('home');
            }
        }
        return view('home', compact('mining_data', 'mining'));
    }

    public function createType($type) {
        return \App\Type::create([
                    'type_id' => $type->type_id,
                    'icon_id' => isset($type->icon_id) ? $type->icon_id : 0,
                    'group_id' => $type->group_id,
                    'market_group_id' => isset($type->market_group_id) ? $type->market_group_id : 0,
                    'name' => $type->name,
                    'description' => $type->description,
                    'volume' => $type->volume,
                    'volume' => $type->volume,
                    'packaged_volume' => $type->packaged_volume,
        ]);
    }

}
