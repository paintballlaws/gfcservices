<?php

namespace App\Http\Controllers;

use App\User;
use \Sumbria\EveOnline\EveOnline;

class ConnectController extends Controller {

    public function authUrl() {
        $eveonline = new EveOnline();
        $eveonline->setClientId(env('EVEONLINE_CLIENT_ID'));
        $eveonline->setRedirectUri(route('auth.callback'));
        $eveonline->setScope(['esi-industry.read_character_mining.v1', 'esi-industry.read_corporation_mining.v1']);
        $auth_url = $eveonline->getAuthUrl();
        return redirect($auth_url);
    }

    public function callback() {
        $code = isset($_GET['code']) ? $_GET['code'] : '';
        if ($code) {

            // Obtain Access Token
            $eveonline = new EveOnline();
            $eveonline->setClientId(env('EVEONLINE_CLIENT_ID'));
            $eveonline->setClientSecret(env('EVEONLINE_CLIENT_SECRET'));
            $eveonline->generateAccessToken($code);
            $access_token = $eveonline->getAccessToken();
            $refresh_token = $eveonline->getRefreshToken();

            // Obtain Character ID
            $eveonline->setAccessToken($access_token);
            $character = $eveonline->getCharacter();
            $character_id = $character['data']->CharacterID;

            $esi = new \Sumbria\Esi\Esi;
            $esi->setAccessToken($access_token);

            $esi_character_obj = new \Sumbria\Esi\Character;
            $esi_character = $esi_character_obj->getCharacter($character_id);
            $user = User::where('character_id', $character_id)->first();
            $alliance_id = isset($esi_character['data']->alliance_id) ? $esi_character['data']->alliance_id : NULL;
            $user_data = [
                'name' => $esi_character['data']->name,
                'character_id' => $character_id,
                'corporation_id' => isset($esi_character['data']->corporation_id) ? $esi_character['data']->corporation_id : NULL,
                'race_id' => isset($esi_character['data']->race_id) ? $esi_character['data']->race_id : NULL,
                'alliance_id' => $alliance_id,
                'bloodline_id' => isset($esi_character['data']->bloodline_id) ? $esi_character['data']->bloodline_id : NULL,
                'ancestry_id' => isset($esi_character['data']->ancestry_id) ? $esi_character['data']->ancestry_id : NULL,
                'gender' => isset($esi_character['data']->gender) ? $esi_character['data']->gender : NULL,
                'dob' => isset($esi_character['data']->birthday) ? date('Y-m-d H:i:s', strtotime($esi_character['data']->birthday)) : NULL,
                'description' => isset($esi_character['data']->description) ? base64_encode($esi_character['data']->description) : NULL,
                'access_token' => $access_token,
                'refresh_token' => $refresh_token
            ];

            if ($user) {
                $user->update($user_data);
            } else {
                if ($alliance_id == '99004425') {
                    $user_data['group'] = 'member';
                }
                if ($alliance_id == '1354830081') {
                    $user_data['group'] = 'blue';
                }
                $user = User::create($user_data);
            }
            auth()->login($user);
            return redirect()->route('home');
        } else {
            die('Invalid Request.');
        }
    }

}
