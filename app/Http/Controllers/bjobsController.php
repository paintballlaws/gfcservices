<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\bjobs;
use App\Itemlookup;


class bjobsController extends Controller
{
    
	
	public function index()
	{
		
        
		
	}
	
	public function create()
	{
		
		return view('BuildingJobs.create');
		
		
	}
	
	public function show()
	{
		
		$buildingjobs = bjobs::latest()->get();
		
		
		return view('BuildingJobs.show', compact('buildingjobs'));
		
		
	}
	
	
	public function ushow($id)
	{
	
		$buildingjobs = bjobs::find($id);
		
		return view('BuildingJobs.ushow', compact('buildingjobs'));
		
		
	}
	
	public function store(Request $request)
	
		
		{
	
		
		$post = new bjobs;
		
		$post->item_name = $request['item_name'];
		
		$post->user_id = auth()->id();
		
		$post->tips = $request['tips'];
		
		$post->comments_from_user = $request['comments_from_user'];
		
		$post->expedited_job = $request['expedited_job'] == "on" ?1:0;
		
		$post->save();
		session()->flash('alert-success', 'Job Is In The Queue');
		return redirect('/');
		
		
	}
	
	
	public function edit($id) {
        $buildingjobs= bjobs::find($id);
        if ($buildingjobs) {
            return view('BuildingJobs.edit', compact('buildingjobs'));
        } else {
            session()->flash('alert-danger', 'Something went wrong. Please try again.');
            return redirect()->route('/bjobs/show');
        }
    }
	
	
	public function update(Request $request, $id) {
        
		$buildingjobs = bjobs::find($id);
		$buildingjobs->item_name=$request->get('item_name');
		$buildingjobs->status=$request->get('status');
		$buildingjobs->builder_comments=$request->get('comments_from_builder');
		
		$buildingjobs->save();
		return redirect()->route(BuildingJobs.table)
			->with('success', 'Job Updated Successfully');

    }
	
	public function destroy($id)
	{
		
		$buildingjobs = bjobs::find($id);
		$buildingjobs->delete();
		
		return redirect()->route('BuildingJobs.table')
			->with('success', 'Job deleted successfully');
		
		
	}
	
	
	
	public function lookupitemname($searchTerm)
		{
    		$itemsFound = Itemlookup::where('item_name', 'like', '%'.$searchTerm.'%')->get();
    		return response()->json(['error' => false, 'items' => $itemsFound]);;
		}
	
}
