<?php

namespace App\Http\Controllers;

class MainPageController extends Controller {

    public function index() {
        return view('MainPage.index');
    }

    public function changelog() {
        return view('MainPage.changelog');
    }
	
	public function bastion() {
        return view('bastion.welcome');
    }
	
	public function bug() {
        return view('MainPage.bug');
    }
	
	public function noauth() {
        return view('MainPage.notauthorized');
    }

}
