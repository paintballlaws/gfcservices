<?php

namespace App\Http\Middleware;

use Closure;

class ShowBuildingMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (auth()->check()) {
            if (auth()->user()->group == 'builder') {
                return $next($request);
            } elseif (auth()->user()->group == 'admin') {
                return $next($request);
            } elseif (auth()->user()->group == 'owner') {
                return $next($request); 
			} else {
                return redirect()->route('noauth');
            }
        } else {
            return redirect()->route('login');
        }
    }

}
