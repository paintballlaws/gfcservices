<?php

namespace App\Http\Middleware;

use Closure;

class BlueMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (auth()->check()) {
            if (auth()->user()->group == 'blue') {
                return $next($request);
            } else {
                return redirect()->route('noauth');
            }
        } else {
            return redirect()->route('login');
        }
    }

}
