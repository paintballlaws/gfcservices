<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model {

    protected $fillable = [
        'system_id', 'star_id', 'name'
    ];

}
