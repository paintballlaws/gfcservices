(function ($) {
    $(document).ready(function () {
        var site_url = $('meta[name="site-url"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Update Is Active
        $(document).on('change', '.update_is_active', function () {
            toastr.remove();
            var el = $(this);
            var table = el.data('table');
            var id = el.data('id');
            var is_active = 0;
            if (el.find('.is_active').is(':checked')) {
                is_active = 1;
            }
            var loader = el.siblings('.is_active_loader');
            loader.removeClass('m--hide');
            $.ajax({
                url: site_url + '/backend/common/update/active/' + table + '/' + id,
                type: 'post',
                data: {
                    _method: 'patch',
                    is_active: is_active,
                },
                success: function (response) {
                    loader.addClass('m--hide');
                    if (response.status === 'success') {
                        toastr.success('Status changed successfully.');
                        el.removeClass('m--hide');
                    } else {
                        toastr.error('Something went wrong.');
                    }
                }
            });
        });

        // Delete List Item
        $(document).on('click', '.delete_list_item', function(){
            toastr.remove();
            var el = $(this);
            var table = el.data('table');
            var id = el.data('id');
            swal({
                title:"Are you sure?",
                text:"You won't be able to revert this!",
                type:"warning",
                showCancelButton: true,
                confirmButtonText:"Delete",
                preConfirm: () => {
                    return new Promise((resolve) => {
                        $.ajax({
                            url: site_url + '/backend/common/delete/' + table + '/' + id,
                            type: 'POST',
                            data: {
                                _method: 'delete'
                            },
                            success: function (response) {
                                if (response.status === 'success') {
                                    swal.close();
                                    toastr.success('Item deleted successfully.');
                                    el.closest('tr').hide(400, function(){
                                        $(this).remove();
                                    });
                                } else {
                                    toastr.error('Something went wrong.');
                                }
                            }
                        });
                    })
                },
            });
        })
    });
})(jQuery);