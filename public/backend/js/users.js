(function ($) {
    $(document).ready(function () {
        $(document).on('change', '#sign_in_options', function () {
            var option = $(this).val();
            if (option === 'username') {
                $('.username_section').removeClass('m--hide');
                $('.email_section').addClass('m--hide');
            } else {
                $('.email_section').removeClass('m--hide');
                $('.username_section').addClass('m--hide');
            }
        });
    });
})(jQuery);
